# HOOKS

It is possible to let the build script execute scripts at defined points of the workflow.

## Add a hook

To add a hook, place a script named like the hook, ending is .sh in the `${project_folder}/hooks` directory

## List of Hooks

- pre_check
- post_check
- pre_build
- post_build
- pre_test
- post_test
- pre_deploy
- post_deploy
