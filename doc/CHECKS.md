# CHECKS

You can add check scripts to your autobuilder project to determine whether a pipeline should run.

### Create Checks

A Check is a shell script located under `autobuilderx/checks/[script-name].check.sh`
It is picked up by the check stage and executed.

#### Example

```sh
#!/bin/sh

# Get the version of an app from a git tag
version_from_tag=$(git ls-remote --refs --tags "${repository}" | sed 's/.*tags\/\(v[0-9]\.[0-9.]\+\).*$/\1/g' | sort -u -r | head -n 1)

if [ -z "$version_from_tag" ]; then
  log 3 "cannot find version"
  exit 1
fi

# Check existing tags in container_ci repository contain the $version_from_tag
existing_tags=$(git ls-remote -h -t --refs origin "${version_from_tag}" | wc -l)

if [ "$existing_tags" -gt 0 ]; the
  # cancel pipeline gracefully if tag exists already
  return 42
else
  # run pipeline if tag does not exist
  return 18
fi


```

### Status Codes

In the script you should return a status code, based on the outcome of your check.

#### Status Code 0

The check ran and nothing special should be done. The other checks and the pipeline will continue

#### Status Code 1

The check returned with an error. The pipeline immediately exits with an error.

#### Status Code 18

The check indicates that the pipeline should run, even in a later check wants to cancel the pipeline.
The other checks continue as normal and the pipeline continues if no other check returns Status Code 1 or 43.

#### Status Code 19

The check indicates that the pipeline should run and no other checks should be executed.

_use with caution_

#### Status Code 42

The check indicates that the pipeline should be canceled gracefully without failure.
The other checks continue as normal and the pipeline is canceled afterwards if no checks returned 1, 18 or 19

#### Status Code 43

The check indicates that the pipeline should be canceled gracefully without failure immediately.
No other checks should be executed.

_use with caution_
