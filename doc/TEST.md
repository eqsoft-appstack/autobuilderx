# TEST

Tests can be executed during the workflow.
To add tests simply create shell scripts `${project_folder}/test/my-test-name.test.sh`.
The workflow detects the scripts in the `test/` folder ending with `*.test.sh` and executes them.

## Test conventions

### Exit codes

To determine whether a test was successful, the workflow reads the exit code of the test shell script.
Exit codes greater than 0 are considered test failures

### available environment variables for testing

#### ${test_name}

Name of the test useful to distinguish from other tests.
(e.g. to separate test data files `${cache_folder}/${test_name}/my.conf`)

#### ${free_port}

Free port to use for the docker image.
(e.g. if ports are published in docker run the container can be accessed by http://docker:PUBLISHED_PORT/)

`docker` is the fix hostname of the dind docker server, where everything runs. For more informations about runners docker architecture read

#### ${test_container_name}

Unique name for the test container.
Please use it for the docker run command:

```sh
docker run [...] --name "${test_container_name}" [...]
```

#### ${target_image} and ${target_tag}

The identification of the image just build in the workflow.

Please use it for the docker run command:

```sh
docker run [...] "${target_image}:${target_tag}"
```
