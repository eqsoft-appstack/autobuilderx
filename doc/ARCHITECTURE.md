# ARCHITECTURE

Excelent overview of docker:dind build concepts: https://blog.hiebl.cc/posts/gitlab-runner-docker-in-docker/

## Applied concept
![image info](doc/images/gitlab-ci-dind.svg)

## Annotation
Deviating from the above-mentioned concept it is possible to use `docker:dind-rootless` instead of `docker:dind` service image in a custom gitrunner to increase the security (https://docs.docker.com/engine/security/rootless/)
