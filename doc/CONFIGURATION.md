# CONFIGURATTION

## CI Configuration

There are two main files to configure the pipeline:

- autobuilderx.cfg
- .gitlab-ci.yml

Checks, hooks and tests can be configured in autobilderx/* folder.

The basic config params, checks and hooks are pre-populated dependant on the template repo from which the repo was forked.

## autobuilderx.cfg

Complete configuration file with detailed informations about the most relevant params: [autobuilderx.cfg](./autobuilderx.cfg)

## .gitlab-ci.yml

Default pipeline trigger is the scheduler. The pipeline triggers can be controlled in .gitlab-ci.yml:

```
...

workflow:
  rules:
    # Trigger rules:
    # The default CI_PIPELINE_SOURCE trigger is schedule
    # Edit or comment in other trigger below

    # Create a pipline on schedules
    - if: $CI_PIPELINE_SOURCE == "schedule"

    # Create a pipline on every push commit
    # - if: $CI_PIPELINE_SOURCE == "push"

    # Create a pipeline on merge_requests_event
    # - if: $CI_PIPELINE_SOURCE == "merge_request_event"

    # Create a pipline on web "Run pipline"
    # - if: $CI_PIPELINE_SOURCE == "web" 

    # Example: Create pipeline on every push on every branch except main branch
    # - if: $CI_COMMIT_BRANCH == "main" && $CI_PIPELINE_SOURCE == "push"
    #   when: never

...
```