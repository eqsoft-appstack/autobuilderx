# Installation

Only required for local or custom gitrunner environments.

## Local Installation

The pipelines can be processed on a local Linux host with docker installed. This is helpfull for build processes with high workloads or local image testing or developing the pipeline itself before installation to remote gitlab server.

Images can be pushed to a remote or local target registry. For a full functional local development it is highly recommanded to run a preconfigured local registry with credentials and TLS enabled.

### Preconfigured Local Registry

"localregistry.example.com" must be resolved to localhost, e.g. by extending localhost entry in /etc/hosts:

```sh
127.0.0.1 localhost localregistry.example.com
```

Add entry to "insecure-registries" in /etc/docker/daemon.json for supporting self-signed certs, restarting docker required:

```json
{
"insecure-registries":["https://localregistry.example.com:5000","localregistry.example.com:5000"],
}
```

Clone registry docker-compose repo:

```sh
git clone https://gitlab.com/eqsoft-appstack/registry.git localregistry
cd localregistry
./up.sh
```

An external docker network "cibuilder" will be created (if not already exists) that is shared by registry and buildx service containers for direct network access. 
The buildx container can also access the registry container by the hostname alias in the shared cibuilder network:

```yaml
networks:
    default:
        aliases:
          - "localregistry.example.com"
```

### Local Installation of autobuilderx

Get autobuilderx and install to `$HOME/.local/bin/`:

```sh
git clone https://gitlab.com/eqsoft-appstack/autobuilderx.git autobuilderx
cd autobuilderx
./install.sh
autobuilderx --version
 autobuilderx 2.5.4
```



```sh
## TARGET IMAGE
target_registry='localregistry.example.com:5000'
target_registry_user='admin'
target_registry_pass='password'
```

## Development

### Variables

- Uppercase Variables are environment vars
- lowercase Variables are specific to the script
- lowercase Variables ending with `*_arg` are there to override variables via comandline arg

### Linting

#### shellcheck

Shellcheck is configured to run in the gitlab-ci on merge-requests

##### Special cases

###### Shellcheck Error: [SC2154](https://www.shellcheck.net/wiki/SC2154)

By separating functions in different files, we have the problem that many variables seem refrenced but not assigned.
Fix this by checking if the variable is really assigned in another part of the script and than add `${variable:?}` to fail if the variable is not assigned.
If the variable is allowed to be empty use `${variable:-}` instead
Make sure the variable is spelled right, before using any of the aforementioned solutions.

###### Shellcheck Error: [SC2034](https://www.shellcheck.net/wiki/SC2034)

By separating functions in different files, sometimes variables are assigned but not used.
Check if the variable are really used at another point and add `# shellcheck disable=SC2034 ` at the line above.
Make sure the variable is spelled right

#### pre-commit

Please use the pre-commit hooks supplied by `.pre-commit-config.yaml`.

##### Usage

1. Install pre-commit: [pre-commit.com/#install](https://pre-commit.com/#install)
2. Install the pre-commit hook: In the autobuilderx folder execute: `pre-commit install`
