#!/bin/sh

bin_folder="$HOME/.local/bin"
config_folder="$HOME/.config/autobuilderx"

if [ ! -d "${bin_folder}" ]; then
  mkdir -p "${bin_folder}"
fi

if [ -f "${bin_folder}/autobuilderx" ] || [ -d "${bin_folder}/abxlib" ]; then
  echo "autobuilderx already installed in $bin_folder"

  echo "Overwrite existing autobuilderx?"
  echo "[y/n]: "

  read -r overwrite_existing_autobuilderx

  cwd=$(pwd)
  case "$overwrite_existing_autobuilderx" in
    y | Y)
      echo "delete existing autobuilderx"
      cd "${bin_folder}" || exit 1
      rm -r ./abxlib
      rm ./autobuilderx
      cd "$cwd" || exit 1
      ;;
    *)
      echo "canceled"
      exit 0
      ;;
  esac
fi

echo "install autobuilderx"

cp ./bin/autobuilderx "${bin_folder}/"
chmod 755 "${bin_folder}/autobuilderx"
cp -r ./bin/abxlib "${bin_folder}/"
chmod -R 755 "${bin_folder}/abxlib"

if [ ! -d "${config_folder}" ]; then
  mkdir -p "${config_folder}"
fi

echo "installed:"
autobuilderx --version
