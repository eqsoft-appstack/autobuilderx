#!/bin/sh

gracefully_cancel_pipeline='n'

find_checks() {
  log 7 "start: find_checks"
  if ! [ -d "${checks_folder:?}" ]; then
    log 4 "no check folder found. skip checks"
    return
  fi
  log 6 "find checks in ${checks_folder} with regex: ${checks_folder}/${check_files_regex:?}"
  log 7 "find command: find \"${checks_folder}\" -regex \"${checks_folder}/${check_files_regex}"
  if [ -z "${check_files_to_run}" ]; then
    check_files_to_run=$(find "${checks_folder}" -regex "${checks_folder}/${check_files_regex}" | sort)
  fi
  log 6 "check_files_to_run: ${check_files_to_run}"
}

run_checks() {
  log 7 "start: run checks"

  if [ -z "$check_files_to_run" ]; then
    echo "no checks to run"
    return
  fi

  for check_file in $check_files_to_run; do
    log 7 "run check file ${check_file}"

    if ! [ -f "${check_file}" ]; then
      log 4 "check file ${check_file} does not exist. skipping"
      continue
    fi
    # login to base and to target registry (to perform checks on private images)
    login_to_base_registry
    login_to_target_registry

    # shellcheck disable=SC1090
    . "${check_file}"

    STATUS=$?

    case "$STATUS" in
      1)
        log 3 "check failed. exit with error. See output above"
        exit 1
        ;;
      18)
        log 6 "check ok"
        ;;
      19)
        log 6 "check ok, abort further checks."
        break;
        ;;
      42)
        log 6 "check failed, abort pipeline gracefully"
        gracefully_cancel_pipeline='y'
        ;;
      43)
        log 6 "check failed, abort pipeline gracefully, abort further checks"
        gracefully_cancel_pipeline='y'
        break;
        ;;
      0)
        log 6 "check returned nothing special, continue"
        ;;
      *)
        log 3 "check returned unknown exit code:${STATUS}, abort"
        exit 1
        ;;
    esac

  done
}

check() {
  if [ "${check_stage:?}" != "1" ]; then
    log 5 "check stage skipped"
    return
  fi
  hook 'pre_check'
  log 6 "stage: check"

  find_checks

  run_checks

  if [ "${gracefully_cancel_pipeline}" = 'y' ]; then
    if [ "${build_force}" = "1" ]; then
      log 6 "build force: pipeline not canceled"
    else
      if ! cancel_pipeline; then
        log 3 "something went wrong during pipeline cancelation"
        exit 1
      fi
      return
    fi
  fi

  hook 'post_check'
}
