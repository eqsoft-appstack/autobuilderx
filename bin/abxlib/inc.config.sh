#!/bin/sh

check_dependencies() {
  for dep in ${dependencies:?}; do
    if ! command -v "$dep" >/dev/null 2>&1; then
      log 3 "command $dep could not be found, but is needed to run $(basename "${0}")"
      log 6 "please check usage:"
      usage
      exit 2
    fi
  done
}

check_arg() {
  if [ -z "${1:-}" ]; then
    log 3 "Missing value for ${2:-parameter}"
    usage
    exit 2
  fi
}

override_defaults_with_config_file() {
  log 7 "start: override_defaults_with_config_file"
  if [ -n "${config_file:-}" ] && [ -f "${config_file}" ]; then
    log 6 "Use ${config_file} config file"
    # shellcheck source=inc.defaults.sh
    . "${config_file}"
    return
  fi

  log 7 "check for global config file"
  if [ -f "$HOME/.config/${default_config_path:?}" ]; then
    global_config_file="$HOME/.config/${default_config_path}"
    log 6 "Use ${global_config_file} as global_config_file"
    # shellcheck source=inc.defaults.sh
    . "${global_config_file}"
  elif [ -f "$XDG_CONFIG_HOME/${default_config_path:?}" ]; then
    global_config_file="${XDG_CONFIG_HOME}/${default_config_path}"
    log 6 "Use ${global_config_file} as global_config_file"
    # shellcheck source=inc.defaults.sh
    . "${global_config_file}"
  fi

  log 7 "check for local config file"
  if [ -f "${project_folder}/${default_local_config_file:?}" ]; then
    config_file="${project_folder}/${default_local_config_file}"
    log 6 "Found local config. Use ${config_file} as local config_file"
    # shellcheck source=inc.defaults.sh
    . "${config_file}"
  fi
}

override_config_with_environment() {
  log 7 "start: override_config_with_environment"

  ## SCRIPT CONFIG
  verbosity=${VERBOSITY:-$verbosity}
  if [ "$verbosity" -gt 7 ]; then
    log 7 "verbosity: $verbosity"
    log 7 "+++ START: PRINTENV ++++"
    printenv
    log 7 "+++ END: PRINTENV ++++"
  fi
  test_run=${TEST_RUN:-$test_run}
  local_only=${LOCAL_ONLY:-$local_only}
  docker_run_options=${DOCKER_RUN_OPTIONS:-$docker_run_options}
  insecure_tls=${INSECURE_TLS:-$insecure_tls}
  pipeline_env=${PIPELINE_ENV:-$pipeline_env}
  pipeline_mode=${PIPELINE_MODE:-$pipeline_mode}
  
  check_stage=${CHECK_STAGE:-$check_stage}
  build_stage=${BUILD_STAGE:-$build_stage}
  build_force=${BUILD_FORCE:-$build_force}
  synch_stage=${SYNCH_STAGE:-$synch_stage}
  test_stage=${TEST_STAGE:-$test_stage}
  deploy_stage=${DEPLOY_STAGE:-$deploy_stage}

  ## BASE IMAGE
  base_registry=${BASE_REGISTRY:-$base_registry}
  base_registry_user=${BASE_REGISTRY_USER:-$base_registry_user}
  base_registry_pass=${BASE_REGISTRY_PASS:-$base_registry_pass}
  base_image_path=${BASE_IMAGE_PATH:-$base_image_path}
  base_tag=${BASE_TAG:-$base_tag}

  ## TARGET IMAGE
  target_registry=${TARGET_REGISTRY:-$target_registry}
  target_registry_user=${TARGET_REGISTRY_USER:-$target_registry_user}
  target_registry_pass=${TARGET_REGISTRY_PASS:-$target_registry_pass}
  target_image_path=${TARGET_IMAGE_PATH:-$target_image_path}
  target_tag=${TARGET_TAG:-$target_tag}

  ## DOCKER BUILD CONFIG
  docker_tls_config=${DOCKER_TLS_CONFIG:-$docker_tls_config}
  additional_tags=${ADDITIONAL_TAGS:-$additional_tags}
  date_tag=${DATE_TAG:-$date_tag}
  date_tag_with_time=${DATE_TAG_WITH_TIME:-$date_tag_with_time}
  minor_tag_regex=${MINOR_TAG_REGEX:-$minor_tag_regex}
  build_platforms=${BUILD_PLATFORMS:-$build_platforms}
  proxies=${PROXIES:-$proxies} # maybe use https_proxy env var?
  container_file=${CONTAINER_FILE:-$container_file}
  build_args=${BUILD_ARGS:-$build_args}
  ci_builder_name=${CI_BUILDER_NAME:-$ci_builder_name}
  build_tag=${BUILD_TAG:-$build_tag}
  synch_tag=${SYNCH_TAG:-$synch_tag}
  add_branch_name_to_tags=${ADD_BRANCH_NAME_TO_TAGS:-$add_branch_name_to_tags}
  add_commit_sha_to_tags=${ADD_COMMIT_SHA_TO_TAGS:-$add_commit_sha_to_tags}
  use_cache=${USE_CACHE:-$use_cache}
  use_cache=${use_cache:-1}
  run_qemu_user_static=${RUN_QEMU_USER_STATIC:-$run_qemu_user_static}
  run_qemu_user_static=${run_qemu_user_static:-1}

  ## GITLAB API CONFIG
  gitlab_private_token=${GITLAB_PRIVATE_TOKEN:-$gitlab_private_token}
  gitlab_server_host=${CI_SERVER_HOST:-$gitlab_server_host}
  gitlab_project_id=${CI_PROJECT_ID:-$gitlab_project_id}
  gitlab_api_version=${GITLAB_API_VERSION:-$gitlab_api_version}

  ## TEST
  test_files_regex=${TEST_FILES_REGEX:-$test_files_regex}
  test_files_to_run=${TEST_FILES_TO_RUN:-$test_files_to_run}

  ## CHECK
  check_files_regex=${CHECK_FILES_REGEX:-$check_files_regex}
  check_files_to_run=${CHECK_FILES_TO_RUN:-$check_files_to_run}

}

override_config_with_args() {
  log 7 "start: override_config_with_args"
  test_run=${test_run_arg:-$test_run}
  local_only=${local_only_arg:-$local_only}
  insecure_tls=${insecure_tls_arg:-$insecure_tls}
  build_force=${build_force_arg:-$build_force}
  pipeline_env=${pipeline_env_arg:-$pipeline_env}
  container_file=${container_file_arg:-$container_file}
  build_args=${build_args_arg:-$build_args}
}

setup_environment() {
  
  check_dependencies

  # Get configuration
  override_defaults_with_config_file
  override_config_with_environment
  override_config_with_args

}

while [ $# -gt 0 ]; do
  case $1 in
    -h | --help)
      usage
      exit 2
      ;;
    -v | -vv | -vvv | -vvvv | -vvvvv)
      verbosity=$((verbosity + (${#1} - 1)))
      ;;
    -n | --new)
      shift
      default_config_path=$default_config_create_path
      setup_environment
      #shellcheck source=abxlib/inc.new.sh
      . "${libpath}/inc.new.sh"
      exit 2
      ;;
    -t | --test-run)
      test_run_arg=1
      ;;
    -l | --local-only)
      local_only_arg=1
      ;;
    -k | --insecure-tls)
      insecure_tls_arg=1
      ;;
    -c | --config-file)
      config_file="${2}"
      shift
      ;;
    -d | --dockerfile | --containerfile)
      check_arg "$2" "container_file_arg"
      container_file_arg="$2"
      shift
      ;;
    -e | --build-env)
      case "$2" in
        local | ci) ;;
        *)
          log 3 "Invalid value for pipeline_env: '${pipeline_env}'."
          usage
          exit 2
          ;;
      esac
      pipeline_env_arg="$2"
      shift
      ;;
    -b | --build-arg)
      check_arg "$2"
      build_args_arg="$2 ${build_args_arg:-}"
      shift
      ;;
    -f | --force)
      build_force_arg=1
      ;;
    -s | --stage)
      check_arg "$2"
      stage=$2
      shift
      ;;
    --version)
      echo " $(basename "${0}") ${version:?}"
      exit 2
      ;;
    -*)
      echo "Invalid option: ${1}."
      usage
      exit 2
      ;;
    *)
      if [ -d "$1" ]; then
        project_folder="$1"
        log 6 "project_folder: ${project_folder}"
      else
        echo "Invalid option: ${1}."
        usage
        exit 2
      fi
      ;;
  esac
  shift
done

check_stage_arg() {
  if [ -z "${stage:-}" ]; then
    log 3 "missing required -s | --stage STAGE parameter: check|build|test|deploy|main"
    exit 1
  fi
}

check_stage_arg

setup_project_folder() {
  if [ -z "${project_folder:-}" ]; then
    project_folder="$(pwd)"
  fi
  if [ -d "${project_folder}/autobuilderx" ]; then
    hooks_folder="${project_folder}/autobuilderx/${hooks_folder_location:-hooks}"
    tests_folder="${project_folder}/autobuilderx/${tests_folder_location:-tests}"
    checks_folder="${project_folder}/autobuilderx/${checks_folder_location:-checks}"
  else
    hooks_folder="${project_folder}/${hooks_folder_location:-hooks}"
    tests_folder="${project_folder}/${tests_folder_location:-tests}"
    checks_folder="${project_folder}/${checks_folder_location:-checks}"
  fi
  if [ -f "${project_folder}/autobuilderx/buildkitd.toml" ]; then
    custom_buildkitd_config="--config ${project_folder}/autobuilderx/buildkitd.toml"
  fi
}

setup_project_folder


gather_target_path() {
  log 7 "start: gather_target_path"
  if [ -n "$target_image_path" ]; then
    log 5 "target_image_path set trough config: '$target_image_path'."
    return
  fi

  if [ -z "${CI_PROJECT_PATH}" ]; then

    go_back="$(pwd)"

    cd "$project_folder" || (log 3 "something went wrong while accessing ${project_folder}" && exit 1)

    _repo=$(git config --get remote.origin.url)
    _group=$(dirname "${_repo}" | sed 's/^.*\///')
    _project=$(basename -s .git "${_repo}")

    target_image_path="${_group}/${_project}"
    log 5 "got target_image_path from repo: '$target_image_path'"

    cd "$go_back" || (log 3 "something went wrong while accessing ${go_back}" && exit 1)
  else
    _group=$(dirname "${CI_PROJECT_PATH}" | sed 's/^.*\///')
    target_image_path="${_group}/${CI_PROJECT_NAME}"

    log 5 "got target_image_path from ci: '$target_image_path'"

  fi
}

check_container_file() {
  log 7 "start: check_container_file"
  if [ "${pipeline_mode:build}" == "synch" ]; then
    log 5 "no container_file required in synch pipeline"
    return
  fi
  if [ -z "$container_file" ]; then
    for container_file_candidate in "Containerfile" "Dockerfile"; do
      log 7 "check container_file_candidate: '$container_file_candidate'"
      if [ -f "${project_folder}/${container_file_candidate}" ]; then
        container_file="${container_file_candidate}"
        log 5 "found container_file: '$container_file'."
        return
      fi
    done
  else
    log 5 "container_file set trough config: '$container_file'"
  fi

  if ! [ -f "${project_folder}/${container_file}" ]; then
    log 3 "no containerfile found"
    exit 1
  fi

}

url_encode_project() {
  log 7 "urlencode project"
  go_back="$(pwd)"
  cd "$project_folder" || exit 1
  repository=$(git remote get-url origin | sed -e 's/git@[^:]\+:\(.*\)\.git/\1/g')
  repository_url_encoded=$(echo "$repository" | jq -sRr @uri | sed 's/%0A//')
  log 6 "urlencoded project: ${repository_url_encoded}"
  cd "$go_back" || exit 1
}

gather_gitlab_config() {
  log 7 "start: gather_gitlab_config"
  if [ -z "$gitlab_private_token" ]; then
    gitlab_api_auth_header="JOB-TOKEN: ${CI_JOB_TOKEN:-}"
  else
    gitlab_api_auth_header="PRIVATE-TOKEN: ${gitlab_private_token}"
  fi
  if [ -z "$gitlab_server_host" ]; then
    go_back="$(pwd)"
    cd "$project_folder" || exit 1
    gitlab_server_host=$(git config --get remote.origin.url | sed 's/git@\(.*\):.*/\1/')
    log 5 "found gitlab_host ${gitlab_server_host}"
    cd "$go_back" || exit 1
  fi
  if [ -z "$gitlab_project_id" ]; then
    url_encode_project
    gitlab_project_id="${repository_url_encoded}"
    log 5 "found project_id ${gitlab_project_id}"
  fi
}

gather_image_informations() {
  log 7 "start: gather_image_informations"
  if [ -z "${base_image_path}" ]; then
    log 3 "base_image_path must not be empty"
    exit 1
  fi

  if [ -z "${target_image_path}" ]; then
    log 3 "target_image_path must not be empty"
    exit 1
  fi

  if [ -z "${base_tag}" ]; then
    log 3 "base_tag must not be empty"
    exit 1
  fi

  if [ -z "${target_tag}" ]; then
    log 3 "target_tag must not be empty"
    exit 1
  fi

  base_image=${base_registry}/${base_image_path}
  target_image=${target_registry}/${target_image_path}
  target_tag=${target_tag:-$base_tag}
  build_tag=${build_tag:-"build-$target_tag"}
  synch_tag=${synch_tag:-"synch-$target_tag"}
  if [ "${pipeline_mode:-}" = "build" ]; then
    pipeline_tag=$build_tag
  else
    pipeline_tag=$synch_tag
  fi
}

check_pipeline_env() {
  log 7 "start: check_pipeline_env"
  
  if [ -z "$pipeline_env" ]; then
    if [ -z "${CI_JOB_ID:-}" ]; then
      pipeline_env='local'
      current_branch=$(git branch --show-current | xargs)
      current_commit=$(git rev-parse HEAD | xargs)
    else
      pipeline_env='ci'
      current_branch=${CI_COMMIT_REF_NAME:-$CI_MERGE_REQUEST_TARGET_BRANCH_NAME}
      current_commit=${CI_COMMIT_SHA}
    fi
  fi
  log 6 "pipeline_env ${pipeline_env}"

  case "$pipeline_env" in
    ci | CI)
      build_host='docker'
      cache_folder='./cache'
      if [ ! -d "$cache_folder" ]; then
        log 6 "$cache_folder not exists...create"
        mkdir "$cache_folder"
      fi
      ;;
    *)
      build_host=127.0.0.1
      cache_folder='/tmp'
      ;;
  esac

  log 6 "use cache_folder: ${cache_folder}"
  log 6 "use build_host: ${build_host}"

  if [ "${add_branch_name_to_tags:-}" = "prefix" ]; then
    target_tag=${current_branch}-${target_tag}
    pipeline_tag=${current_branch}-${pipeline_tag}
  fi
  if [ "${add_branch_name_to_tags:-}" = "suffix" ]; then
    target_tag=${target_tag}-${current_branch}
    pipeline_tag=${pipeline_tag}-${current_branch}
  fi

  if [ "${add_commit_sha_to_tags:-}" = "prefix" ]; then
    target_tag=${current_commit}-${target_tag}
    pipeline_tag=${current_commit}-${pipeline_tag}
  fi
  if [ "${add_commit_sha_to_tags:-}" = "suffix" ]; then
    target_tag=${target_tag}-${current_commit}
    pipeline_tag=${pipeline_tag}-${current_commit}
  fi

  # check integrity
  if [ "${local_only}" = "1" ] && [ "${pipeline_env}" = "ci" ] ; then
    log 3 "in ci pipeline_mode local_only mode is not allowed"
    exit 2
  fi

}

cancel_pipeline() {
  log 4 "cancel pipeline"
  if [ "$pipeline_env" = 'local' ]; then
    local_pipeline_canceled='1'
    return 0
  else
    # shellcheck disable=SC2034  # Unused variables left for readability
    if ! response=$(curl -L -s --request POST --header "${gitlab_api_auth_header}" \
      "https://${gitlab_server_host}/api/${gitlab_api_version}/projects/${gitlab_project_id}/pipelines/${CI_PIPELINE_ID}/cancel"); then
      log 3 "Something went wrong while cancel pipeline through the api"
      exit 1
    fi
    return 0
  fi
}

load_pipeline_cache() {
  log 7 "start: load_pipeline_cache in pipeline_env $pipeline_env"
  if [ "$pipeline_env" = 'local' ]; then
    if [ -z "$cache_file" ]; then
      cache_file=${cache_folder}/$(
        tr -dc A-Za-z0-9 </dev/urandom | head -c 13
        echo
      )
      touch "$cache_file"
      chmod 755 "$cache_file"
    fi
  else
    cache_file=${cache_folder}/${CI_PIPELINE_ID}
    if [ ! -f "$cache_file" ]; then
      touch "$cache_file"
      chmod 755 "$cache_file"
    fi
  fi
  if [ ! -f "$cache_file" ]; then
    log 3 "cache_file $cache_file not exists"
  fi
  # shellcheck disable=SC1090
  . "$cache_file"
}

set_cache_entry() {
  if [ $# -lt 2 ]; then
    log 3 "cache needs key and value args"
    exit 1
  fi
  if [ ! -f "$cache_file" ]; then
    log 3 "cache_file $cache_file not exists"
  fi
  # delete old entry if exists
  sed -i "s/$1=.*//" "$cache_file"
  # create key=value entry
  echo "${1}=${2}" >>"$cache_file"
  # shellcheck disable=SC1090
  . "$cache_file"
}

hook() {
  hook_name="$1"
  log 7 "check hook: ${hook_name}"
  hook="${hooks_folder}/${hook_name}.sh"
  if [ -f "${hook}" ]; then
    log 6 "execute hook '${hook_name}': ${hook}"
    # shellcheck disable=SC1090
    . "${hook}"
  fi
}

log_config() {
  if [ "${verbosity}" -lt 6 ]; then
    return
  fi
  log 6 "project_folder - $project_folder"
  log 6 "hooks_folder   - $hooks_folder"
  log 6 "tests_folder   - $tests_folder"
  log 6 "checks_folder   - $checks_folder"
  log 6 "custom_buildkitd_config" - $custom_buildkitd_config
  log 6 "logging config - base_registry: ${base_registry}"
  log 6 "logging config - base_registry_user: ${base_registry_user}"
  log 6 "logging config - base_registry_pass: **********"
  log 6 "logging config - base_image_path: ${base_image_path}"
  log 6 "logging config - base_tag: ${base_tag}"
  log 6 "logging config - target_registry: ${target_registry}"
  log 6 "logging config - target_registry_user: ${target_registry_user}"
  log 6 "logging config - target_registry_pass: **********"
  log 6 "logging config - target_image_path: ${target_image_path}"
  log 6 "logging config - target_tag: ${target_tag}"
  log 6 "logging config - test_run: ${test_run}"
  log 6 "logging config - local_only: ${local_only}"
  log 6 "logging config - docker_run_options: ${docker_run_options}"
  log 6 "logging config - insecure_tls: ${insecure_tls}"
  log 6 "logging config - build_force: ${build_force}"
  log 6 "logging config - pipeline_env: ${pipeline_env}"
  log 6 "logging config - pipeline_mode: ${pipeline_mode}"
  log 6 "logging config - current_branch: ${current_branch}"
  log 6 "logging config - current_commit: ${current_commit}"
  log 6 "logging config - build_args: ${build_args}"
  log 6 "logging config - docker_tls_config: ${docker_tls_config}"
  log 6 "logging config - gitlab_private_token: **********"
  log 6 "logging config - gitlab_server_host: ${gitlab_server_host}"
  log 6 "logging config - gitlab_project_id: ${gitlab_project_id}"
  log 6 "logging config - gitlab_api_version: ${gitlab_api_version}"
  log 6 "logging config - hash_variable_description: ${hash_variable_description}"
  log 6 "logging config - proxies: ${proxies}"
  log 6 "logging config - container_file: ${container_file}"
  log 6 "logging config - test_files_to_run: ${test_files_to_run}"
  log 6 "logging config - additional_tags: ${additional_tags}"
  log 6 "logging config - date_tag: ${date_tag}"
  log 6 "logging config - date_tag_with_time: ${date_tag_with_time}"
  log 6 "logging config - build_platforms: ${build_platforms}"
  log 6 "logging config - build_tag: ${build_tag}"
  log 6 "logging config - synch_tag: ${synch_tag}"
  log 6 "logging config - add_branch_name_to_tags: ${add_branch_name_to_tags}"
  log 6 "logging config - add_commit_sha_to_tags: ${add_commit_sha_to_tags}"
  log 6 "logging config - use_cache: ${use_cache}"
  log 6 "logging config - run_qemu_user_static: ${run_qemu_user_static}"
  log 6 "logging config - base_image: ${base_image}"
  log 6 "logging config - target_image: ${target_image}"
  log 6 "logging config - target_tag: ${target_tag}"
  log 6 "logging config - check_stage: ${check_stage}"
  log 6 "logging config - build_stage: ${build_stage}"
  log 6 "logging config - synch_stage: ${synch_stage}"
  log 6 "logging config - build_force: ${build_force}"
  log 6 "logging config - test_stage: ${test_stage}"
  log 6 "logging config - deploy_stage: ${deploy_stage}"
  # pipeline cache
  log 6 "logging config - cache_minor_tag: ${cache_minor_tag:-}"
}

setup_configuration() {
  log 7 "start: setup_configuration"

  setup_environment

  # gather some configuration
  # gather_target_path
  check_container_file
  gather_gitlab_config
  gather_image_informations
  check_pipeline_env
  load_pipeline_cache
  log_config
}

setup_configuration
