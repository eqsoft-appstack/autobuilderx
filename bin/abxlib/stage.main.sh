#!/bin/sh

# only for local usage
main() {
  log 6 "stage: main"
  case ${pipeline_mode:?} in
    build)
      check
      build
      test
      deploy
      ;;
    synch)
      check
      synch
      test
      deploy
      ;;
    *)
      log 3 "pipeline_mode not supported: ${pipeline_mode:?}"
      exit 1
    ;;
  esac
}
