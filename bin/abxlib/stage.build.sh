#!/bin/sh

create_builder() {
  if [ "${local_pipeline_canceled:-}" = "1" ]; then
    log 5 "local pipeline canceled"
    return
  fi
  log 7 "start: create builder"
  # this only works in local pipeline_env
  if docker buildx ls | grep -q "${ci_builder_name:?}"; then
    log 4 "CI Builder '${ci_builder_name:?}' exits, skip creating a new one"
  else
    if [ -n "$docker_tls_config" ]; then
      log 6 "create CI Builder '${ci_builder_name:?}' with docker-tls ${docker_tls_config}"
      docker context create docker-tls --docker "$docker_tls_config"
      docker context use docker-tls
      docker buildx create --name="${ci_builder_name:?}" --use docker-tls $custom_buildkitd_config --driver docker-container --driver-opt "env.http_proxy=${proxies:?}" --driver-opt "env.HTTP_PROXY=${proxies:?}" --driver-opt "env.https_proxy=${proxies:?}" --driver-opt "env.HTTPS_PROXY=${proxies:?}"
      docker buildx inspect --bootstrap
    else
      log 6 "create CI Builder '${ci_builder_name:?}'"
      docker network ls | grep cibuilder || docker network create cibuilder
      docker buildx create --name="${ci_builder_name:?}" --driver docker-container --driver-opt "network=cibuilder" --use --config "${libpath}"/buildkitd.local.toml
      docker buildx inspect --bootstrap
    fi
  fi
}

prepare_build_args() {
  build_args_buildx_argument=''
  for build_arg in ${build_args:-}; do
    build_args_buildx_argument="${build_args_buildx_argument} --build-arg ${build_arg}" # @ TODO: not the best way. Need to change this someday: Ideas: https://mywiki.wooledge.org/BashFAQ/050
  done

  log 6 "build_args_buildx_argument ${build_args_buildx_argument}"
}

build_image() {
  if [ "${local_pipeline_canceled:-}" = "1" ]; then
    log 5 "local pipeline canceled"
    return
  fi
  log 7 "start: build image"

  # pull all required base_images after login
  login_to_base_registry

  platform_array=$(echo "${build_platforms:?}" | sed 's/,/ /g')
  for platform in $platform_array; do
    docker pull --platform "$platform" "${base_image:?}:${base_tag:?}"
    if [ "$?" != "0" ]; then
      log 3 "platform $platform does not exist for ${base_image:?}:${base_tag:?}."
      log 3 "You need to reduce build_platforms to existing platforms p.e. build_platforms='linux/amd64' in autobuilderx.cfg"
      exit 2
    fi
  done

  login_to_target_registry
  no_cache=""
  if [ "${use_cache:?}" = "0" ]; then
    no_cache="--no-cache"
  fi

  if [ "${run_qemu_user_static:?}" = "1" ]; then
    docker run --rm --userns=host --privileged multiarch/qemu-user-static --reset -p yes
  fi

  prepare_build_args

  # no better solution for now:
  # shellcheck disable=SC2086

  output="--push"

  if [ "${local_only}" = "1" ]; then
    output="--load"
  fi

  if [ "${local_only}" = "1" ]; then
    if ! docker buildx build \
      --build-arg "BASE_IMAGE=${base_image}" \
      --build-arg "BASE_TAG=${base_tag}" \
      --build-arg "HTTP_PROXY=${proxies}" \
      --build-arg "HTTPS_PROXY=${proxies}" \
      --build-arg "http_proxy=${proxies}" \
      --build-arg "https_proxy=${proxies}" \
      ${build_args_buildx_argument:-} \
      --tag "${target_image}:${pipeline_tag:?}" \
      --file "${container_file:?}" \
      ${output} \
      .; then
      log 3 "Build failed"
      exit 1
    fi
  else
    if ! docker buildx build \
      --platform "${build_platforms}" \
      --build-arg "BASE_IMAGE=${base_image}" \
      --build-arg "BASE_TAG=${base_tag}" \
      --build-arg "HTTP_PROXY=${proxies}" \
      --build-arg "HTTPS_PROXY=${proxies}" \
      --build-arg "http_proxy=${proxies}" \
      --build-arg "https_proxy=${proxies}" \
      ${build_args_buildx_argument:-} \
      ${no_cache} \
      --cache-to type=inline \
      --cache-from type=registry,ref=${target_image:?}:${target_tag:?} \
      --tag "${target_image}:${pipeline_tag:?}" \
      --file "${container_file:?}" \
      ${output} \
      .; then
      log 3 "Build failed"
      exit 1
    fi
  fi
}

build() {
  if [ "${local_pipeline_canceled:-}" = "1" ]; then
    log 5 "local pipeline canceled"
    return
  fi
  if [ "${build_stage:?}" != "1" ]; then
    log 5 "build stage skipped"
    return
  fi
  hook 'pre_build'
  log 6 "stage: build"
  create_builder
  build_image
  hook 'post_build'
}
