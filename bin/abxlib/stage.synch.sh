#!/bin/sh

synch_image() {
  log 5 "synch image"
  log 5 "copy_tag ${base_tag} to ${synch_tag}"
  src_creds=''
  trg_creds=''
  if [ "${base_registry_user:-}" != '' ] && [ "${base_registry_pass:-}" != '' ]; then
    src_creds="--src-username=${base_registry_user} --src-password=${base_registry_pass}"
  fi
  if [ "${target_registry_user:-}" != '' ] && [ "${target_registry_pass:-}" != '' ]; then
    trg_creds="--dest-username=${target_registry_user} --dest-password=${target_registry_pass}"
  fi
  
  tls_verify=

  if [ "${insecure_tls:?}" = "1" ]; then
    tls_verify="--src-tls-verify=false --dest-tls-verify=false"
  fi

  if ! skopeo copy \
    $src_creds \
    $trg_creds \
    $tls_verify \
    --preserve-digests \
    --all \
    "docker://${base_image}:${base_tag}" \
    "docker://${target_image}:${synch_tag}"; then
    log 3 "something went wrong in synch stage"
    exit 1
  fi
}

synch() {
  if [ "${local_pipeline_canceled:-}" = "1" ]; then
    log 5 "local pipeline canceled"
    return
  fi
  if [ "${synch_stage:?}" != "1" ]; then
    log 5 "synch stage skipped"
    return
  fi
  hook 'pre_synch'
  log 6 "stage: synch"
  synch_image
  hook 'post_synch'
}
