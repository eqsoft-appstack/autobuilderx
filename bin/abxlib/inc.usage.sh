#!/bin/sh

usage() {
  echo ""
  echo ""
  echo ""
  echo "Run autobuilderx in $(pwd)"
  echo " Usage:"
  echo ""
  echo " $(basename "${0}") -s [STAGE] [ARGS?]"
  echo ""
  echo "  Args:"
  echo "   -h, --help                                        --> shows usage"
  echo "   -v                                                --> increases VERBOSITY"
  echo "   -n, --new                                         --> create new autobuilderx project"
  echo "   -c, --config-file [config-file]                   --> sets the path to config file and disables config file discovery"
  echo "   -e, --build-env [pipeline_env]                       --> sets the build environemnt. Allowed envs: local and ci"
  echo "   -d, --dockerfile, --containerfile [Containerfile] --> sets the Containerfile used for building the image"
  echo "   -t, --test-run                                    --> does a test run"
  echo "   -f, --force                                       --> forces a build"
  echo "   -s, --stage                                       --> stage: check|build|test|deploy|main"
  echo "   --version                                         --> shows version"
  echo ""
  echo "Dependencies"
  for dep in ${dependencies:?}; do
    echo " - ${dep}"
  done
}
