#!/bin/sh
# shellcheck disable=SC2034

## NOT CONFIGURABLE
dependencies='jq skopeo docker curl'
dependencies_create='git'
default_config_path="autobuilderx/config.cfg"
default_config_create_path="autobuilderx/create.cfg"
default_local_config_file="autobuilderx.cfg"
version='2.5.4'
cache_folder=''
cache_file=''
custom_buildkitd_config=''
# create new defaults
create_template_repo='gitlab.com/eqsoft-appstack/autobuilderx-template.git'
create_project_name=''
create_project_location=''
create_project_branch='latest'
create_git_clone_protocol='https'
# use default branch from template repo
create_git_template_ref=''
create_simple_template_prefix='https://gitlab.com/'

## SCRIPT CONFIG
verbosity=4
pipeline_env=''
pipeline_mode='build'
current_branch='main'
current_commit=''

# check stage
# For skipping set check_stage=0
# WARNING: if the check stage is skipped no cached version or minor_tag parameter exists for subsequent stages
check_stage=1
# build stage
build_stage=1
# synch stage
synch_stage=1
# force build
build_force=0
# test stage
# For skipping set test=0
test_stage=1
# deploy stage
# For skipping set deploy=0
deploy_stage=1
# tests folder in project folder
tests_folder_location='tests'
# hooks folder in project folder
hooks_folder_location='hooks'
# tests folder in project folder
checks_folder_location='checks'

# misc
log_tab=8
test_run=0
local_only=0
docker_run_options=''
# set to 1 for skopeo commands on private registry with self-signed certs
# p.e. required using the default localregistry.example.com:5000 as target_registry
insecure_tls=0

## BASE IMAGE
base_registry='docker.io'
base_registry_user=''
base_registry_pass=''
base_image_path=''
base_tag=''

## TARGET IMAGE
target_registry='docker.io'
target_registry_user=''
target_registry_pass=''
target_image_path=''
target_tag=''

## DOCKER BUILD CONFIG
docker_tls_config=''
additional_tags=''
date_tag_with_time=''
date_tag=''
pipeline_tag=''
# ''|'prefix'|'suffix'
add_branch_name_to_tags=''
#minor_tag_regex="^[0-9]+\.[0-9]+\.[0-9]+$"
minor_tag_regex=''
build_platforms='linux/amd64,linux/arm64'
proxies=''
container_file=''
build_args=''
ci_builder_name='cibuilder'
use_cache=1

# required on docker executor on gitlab.com
run_qemu_user_static=1

## GITLAB API CONFIG
gitlab_private_token=''
gitlab_server_host=''
gitlab_project_id=''
gitlab_api_version='v4'

## TEST
test_files_regex='.*\.test\.sh'
test_files_to_run=''

## check
check_files_regex='.*\.check\.sh'
check_files_to_run=''

## PIPELINE CACHE
# if checks canceled the local pipeline
local_pipeline_canceled='0'
# cached minor_tag
cache_minor_tag=''
