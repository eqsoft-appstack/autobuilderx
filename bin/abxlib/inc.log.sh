#!/bin/sh

get_loglevel() {
  case $1 in
    0)
      echo "emerg"
      ;;
    1)
      echo "alert"
      ;;
    2)
      echo "crit"
      ;;
    3)
      echo "err"
      ;;
    4)
      echo "warning"
      ;;
    5)
      echo "notice"
      ;;
    6)
      echo "info"
      ;;
    7)
      echo "debug"
      ;;
  esac
}

get_log_tab() {
  num_character=${#1}
  if [ "$num_character" -gt "${log_tab:?}" ]; then
    echo "log_tab must be greater then the character number of the longest log_string"
    exit 1
  fi
  spacing=''
  until [ "$num_character" -eq "${log_tab:?}" ]; do
    num_character=$((num_character + 1))
    if [ $num_character -eq 100 ]; then
      echo "something went wrong in log_tab function"
      exit 1
    fi
    spacing="${spacing} "
  done
  echo "$spacing"
}

log() {
  log_level_value=$1
  shift
  if [ "${verbosity:-7}" -ge "${log_level_value}" ]; then
    log_level_string=$(get_loglevel "${log_level_value}")
    # echo "$(date --iso-8601=minutes) - [${log_level_string}]: " "$@"
    spacing=$(get_log_tab "$log_level_string")
    echo "[${log_level_string}]$spacing- " "$@"
  fi
}
