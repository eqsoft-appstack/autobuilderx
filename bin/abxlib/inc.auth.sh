#!/bin/sh

login_to_base_registry() {
  log 7 "start: login_to_base_registry"
    if [ "${local_only}" = "1" ]; then
    log 5 "local_only, skip login"
    return
  fi
  if [ -n "${base_registry_user}" ] && [ -n "${base_registry_pass}" ]; then
    printf "%s" "${base_registry_pass}" | docker login -u "${base_registry_user}" --password-stdin "${base_registry:?}"
    log 5 "logged into ${base_registry:?}"
    # shellcheck disable=SC2034
    base_registry_auth=1 # dont know if really needed
  fi
}

login_to_target_registry() {
  log 7 "start: login_to_target_registry"
    if [ "${local_only}" = "1" ]; then
    log 5 "local_only, skip login"
    return
  fi
  if [ -n "${target_registry_user}" ] && [ -n "${target_registry_pass}" ]; then
    printf "%s" "${target_registry_pass:?}" | docker login -u "${target_registry_user:?}" --password-stdin "${target_registry:?}"
    log 5 "logged into ${target_registry}"
  fi
}
