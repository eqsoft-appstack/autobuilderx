#!/bin/sh

dependencies='git'

check_create_arg() {
  if [ -z "${1:-}" ]; then
    log 3 "Missing value for ${2:-parameter}"
    create_usage
    exit 2
  fi
}

create() {
  echo "create $2"
}

create_usage() {
  echo ""
  echo "create a autobuilderx fueled Container Project"
  echo ""
  echo " Usage:"
  echo ""
  echo " $(basename "${0}") -n [args?] [project_name] "
  echo ""
  echo "  Args:"
  echo "   -t | --template [template_repo] --> set the template repository"
  echo "   -p | --protocol [git|ssh|https] --> set wheater to clone via ssh or https. default: ${create_git_clone_protocol}. https or git@ create_template_repo urls will override this"
  echo "   -r | --ref [git_ref]            --> set ref of the target template repository. default: ${create_git_template_ref}."
  echo ""
  echo "Dependencies"
  for dep in $dependencies; do
    echo " - ${dep}"
  done
}

while [ $# -gt 0 ]; do
  case $1 in
    -t | --template)
      check_create_arg "$2" "create_template_repo"
      create_template_repo=$2
      shift
      ;;
    -p | --protocol)
      check_create_arg "$2" "git clone method. Allowed are ssh, git (äquivalent to ssh) or https."
      case $2 in
        git | ssh)
          create_git_clone_protocol=git
          ;;
        https)
          create_git_clone_protocol=https
          ;;
        *)
          log 3 "git clone method  $2 not allowed. Allowed are ssh, git (äquivalent to ssh) or https."
          ;;
      esac
      shift
      ;;
    -r | --ref)
      check_create_arg "$2" "git ref"
      create_git_template_ref="$2"
      shift
      ;;
    -*)
      echo "Invalid option: ${1}."
      create_usage
      exit 2
      ;;
    *)
      if [ -z "${create_project_name_arg:-}" ]; then
        create_project_name_arg="$1"
        log 6 "create_project_name_arg: ${create_project_name_arg}"
      else
        echo "Invalid option: ${1}"
        create_usage
        exit 2
      fi
      ;;
  esac
  shift
done

if [ -z "${create_project_name_arg:-}" ]; then
  create_usage
fi

get_create_project_name() {
  log 7 "start get_create_project_name"
  if [ -z "${create_project_name_arg:-}" ]; then
    log 3 "missing create_project_name"
    create_usage
    exit 2
  fi

  create_project_name=$(basename "$create_project_name_arg")
  create_project_location=$(dirname "$create_project_name_arg")

  log 6 "create_project_location: $create_project_location"

  if ! [ -d "$create_project_location" ]; then
    log 3 "cannot find folder $create_project_location to create the project $create_project_name"
    exit 1
  fi

  project_folder="${create_project_location}/${create_project_name}"
  log 6 "project_folder: $project_folder"

  log 7 "end get_create_project_name"
}

get_template_url() {
  log 7 "start get_template_url"
  short_template=$(echo "$create_template_repo" | sed -n '/^[^: .]\+$/p')
  if [ -n "$short_template" ]; then
    create_template_repo="${create_simple_template_prefix}/${create_template_repo}.git"
  fi
  case $create_template_repo in
    git@*)
      create_git_clone_protocol=git
      template_url="${create_template_repo}"
      ;;
    https://)
      create_git_clone_protocol=https
      template_url="${create_template_repo}"
      ;;
    *)
      case $git_clone_method in
        git)
          template_url="git@${template_repo}"
          ;;
        https)
          template_url="https://${template_repo}"
          ;;
      esac
      ;;
  esac
  if [ -z "$template_url" ]; then
    case $create_git_clone_protocol in
      git)
        template_url="git@${create_template_repo}"
        ;;
      https)
        template_url="https://${create_template_repo}"

        ;;
    esac
  fi
  log 6 "template_url: $template_url"
  log 7 "end get_template_url"
}

clone_template() {
  log 7 "start clone template repository ${template_url}"

  branch=''

  if [ -n "${create_git_template_ref}" ]; then
    branch="--branch=$create_git_template_ref"
  fi
  
  if ! git clone $branch "$template_url" "${project_folder}"; then
    log 3 "cannot clone template into ${project_folder}"
    exit 1
  fi

  log 6 "cleanup template repository files"
  rm -rf "${project_folder}/.git"

  log 7 "end clone template repository"
}

copy_with_ask_to_override() {
  file_new=$1
  file_old=${project_folder}/$(basename "$file_new")
  if [ -e "${file_old}" ]; then
    confirm_override='no'
    echo "Should we override $1? [y/n]"
    read -r confirm_override

    if [ "$confirm_override" = 'y' ]; then
      log 7 "copy with confirm  $file_new to $file_old"
      if [ -d "$file_new" ]; then
        cp -r "$file_new" "$file_old"
      else
        cp "$file_new" "$file_old"
      fi
    fi
  else
    log 7 "copy $file_new to $file_old"
    if [ -d "$file_new" ]; then
      cp -r "$file_new" "$file_old"
    else
      cp "$file_new" "$file_old"
    fi
  fi
}

enhance_existing_project() {
  if [ -d "/tmp/${create_project_name}" ]; then
    rm -rf "/tmp/${create_project_name}"
  fi
  branch=''

  if [ -n "${create_git_template_ref}" ]; then
    branch="--branch=$create_git_template_ref"
  fi
  if ! git clone $branch "$template_url" "/tmp/${create_project_name}"; then
    log 3 "cannot clone template into /tmp/${create_project_name}"
    exit 1
  fi

  for file in "/tmp/${create_project_name}"/* "/tmp/${create_project_name}"/.*; do
    log 7 "file: $file"
    case $file in
      *.git)
        log 7 "skip $file"
        ;;
      *)
        log 7 "copy_with_ask_to_override $file"
        copy_with_ask_to_override "$file"
        ;;
    esac
  done

  rm -rf "/tmp/${create_project_name}"
}

init_git_repo() {
  log 7 "start init_git_repo"
  init_git_repo_go_back=$PWD
  cd "$project_folder" || exit 1

  git init --initial-branch="${create_project_branch}"

  cd "$init_git_repo_go_back" || exit 1

  log 7 "end init_git_repo"
}

execute_template_script() {
  log 7 "start execute_template_script"
  if ! [ -f "${project_folder}/template.sh" ]; then
    log 6 "no tempalte script found to execute"
    return
  fi
  init_git_repo_go_back=$PWD
  cd "$project_folder" || exit 1

  export create_project_name
  # shellcheck disable=SC1090
  if ! ./template.sh; then
    log 3 "something went wrong during tempalte script execution"
  fi

  rm template.sh

  cd "$init_git_repo_go_back" || exit 1

  log 7 "end execute_template_script"
}

install_pre_commit_if_present() {
  if [ -f "${project_folder}/.pre-commit-config.yaml" ]; then
    install_pre_commit_if_present_back=$PWD
    cd "$project_folder" || exit 1
    pre-commit install
    cd "$install_pre_commit_if_present_back" || exit 1
  fi
}

get_create_project_name
get_template_url

if [ -d "$project_folder" ]; then
  enhance_existing_project

  execute_template_script

else
  clone_template

  execute_template_script

  init_git_repo
fi

install_pre_commit_if_present
