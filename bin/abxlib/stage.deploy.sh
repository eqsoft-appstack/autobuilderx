#!/bin/sh

copy_tag() {
  _copy_to_tag=$1

  login_data=
  tls_verify=

  if [ -n "${target_registry_user}" ] && [ -n "${target_registry_pass}" ]; then
    login_data="--src-username=${target_registry_user} --src-password=${target_registry_pass} --dest-username=${target_registry_user} --dest-password=${target_registry_pass}"
  fi

  if [ "${insecure_tls:?}" = "1" ]; then
    tls_verify="--src-tls-verify=false --dest-tls-verify=false"
  fi

  if [ "${local_only}" = "1" ]; then
    log 4 "beware that in local_only mode nothing is pushed to the target_registry"
    log 4 "the image is loaded into the local docker storage"
    if ! docker image tag \
      "${target_image:?}:${pipeline_tag:?}" \
      "${target_image}:${_copy_to_tag}"; then
      log 3 "something went wrong in deploy stage"
      exit 1
    fi
  else
    if ! skopeo copy \
      $login_data \
      $tls_verify \
      --preserve-digests \
      --multi-arch index-only \
      "docker://${target_image:?}:${pipeline_tag:?}" \
      "docker://${target_image}:${_copy_to_tag}"; then
      log 3 "something went wrong in deploy stage"
      exit 1
    fi
  fi
}

deploy() {
  if [ "${local_pipeline_canceled:-}" = "1" ]; then
    log 5 "local pipeline canceled"
    return
  fi
  if [ "${deploy_stage:?}" != "1" ]; then
    log 5 "deploy stage skipped"
    return
  fi
  hook 'pre_deploy'
  log 6 "stage: deploy"

  if [ "${test_run:?}" = "1" ]; then
    log 6 "skipping deploy stage in test_run = 1"
    return
    #exit ${CI_JOB_SKIP_EXIT_CODE:-0}
  fi

  log 7 "target_tag: copy ${target_image:?}:${pipeline_tag:?} to ${target_image:?}:${target_tag:?}"
  copy_tag "${target_tag}"

  if [ -n "${cache_minor_tag:-}" ]; then
    minor_tag=${cache_minor_tag}
    if [ "${add_branch_name_to_tags:-}" = "prefix" ]; then
      minor_tag=${current_branch}-${minor_tag}
    fi
    if [ "${add_branch_name_to_tags:-}" = "suffix" ]; then
      minor_tag=${minor_tag}-${current_branch}
    fi
    log 7 "minor_tag: copy ${target_image}:${pipeline_tag} to ${target_image}:${minor_tag}"
    copy_tag "${minor_tag}"
  fi
  
  additional_tags=$(echo -n ${additional_tags/%,\s*/ } | xargs)
  additional_tags=${additional_tags/%\;\s*/ }

  for tag in ${additional_tags:-}; do
    if [ -z "${tag:?}" ]; then
      continue
    fi
    if [ "${add_branch_name_to_tags:-}" = "prefix" ]; then
      tag=${current_branch}-${tag}
    fi
    if [ "${add_branch_name_to_tags:-}" = "suffix" ]; then
      tag=${tag}-${current_branch}
    fi
    log 7 "additional_tag: copy ${target_image}:${pipeline_tag} to ${target_image}:${tag}"
    copy_tag "${tag}"
  done

  if [ "${date_tag:-}" = "1" ]; then
    if [ "${date_tag_with_time:-}" = "1" ]; then
      copy_tag $(date +%F_%H-%M-%S)
    else
      copy_tag $(date +%F)
    fi
  fi

  hook 'post_deploy'
}
