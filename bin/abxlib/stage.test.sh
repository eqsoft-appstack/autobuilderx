#!/bin/sh

find_tests() {
  log 7 "start: find_tests"
  if ! [ -d "${tests_folder}" ]; then
    log 4 "no test folder found. skip tests"
    return
  fi
  log 6 "find tests in ${tests_folder} with regex: ${tests_folder}/${test_files_regex:?}"
  log 7 "find command: find \"${tests_folder}\" -regex \"${tests_folder}/${test_files_regex}"
  if [ -z "${test_files_to_run}" ]; then
    test_files_to_run=$(find "${tests_folder}" -regex "${tests_folder}/${test_files_regex}" | sort)
  fi
  log 6 "test_files_to_run: ${test_files_to_run}"
}

find_free_port() {
  log 7 "start: find_free_port"
  min=1001
  max=9000

  find_free_port_tries=0
  find_free_port_max_tries=100

  while :; do
    free_port=$(($(awk 'BEGIN { srand(); print int(rand()*32768) }' /dev/null) % (max - min + 1) + min))
    log 6 "check port on ${build_host:?}:${free_port}"
    nc -z -w 1 "${build_host}" "${free_port}" || break

    find_free_port_tries=$((find_free_port_tries + 1))
    if [ $find_free_port_tries -eq $find_free_port_max_tries ]; then
      log 3 "no free port found"
      exit 1
    fi
  done

  log 5 "${build_host}:${free_port} is free"
}

test_image() {
  log 7 "start: test image"

  find_tests

  if [ -z "$test_files_to_run" ]; then
    echo "no tests to run"
    return
  fi

  log 7 "start: test_image"

  find_free_port

  # "FREE_PORT: ${free_port}"
  # "can be used as random number for test config files and/or a free bind port if required"
  
  test_image="${target_image:?}:${pipeline_tag:?}"
  login_to_target_registry

  if [ "${local_only}" != "1" ]; then
    docker pull "$test_image"
  fi

  for test_file in $test_files_to_run; do
    test_name=$(basename "${test_file}" | sed 's/[.]/-/g')

    test_cache_folder="${cache_folder}/${test_name}"
    if [ ! -d "${test_cache_folder}" ]; then
      mkdir "${test_cache_folder}"
    fi

    test_container_name=test-image-${test_name}-${free_port}

    log 5 "run test ${test_name}"

    log 5 "test_name: ${test_name}"
    log 5 "cache_folder: ${cache_folder}"
    log 5 "test_cache_folder: ${test_cache_folder}"
    log 5 "tests_folder: ${tests_folder}"
    log 5 "free_port: ${free_port}"
    log 5 "test_container_name: ${test_container_name}"
    log 5 "test_image: ${test_image}"

    if [ -z "${cache_folder}" ] \
      || [ -z "${test_cache_folder}" ] \
      || [ -z "${test_name}" ] \
      || [ -z "${tests_folder}" ] \
      || [ -z "${free_port}" ] \
      || [ -z "${test_image}" ] \
      || [ -z "${test_container_name}" ]; then
      log 3 "please check test environment, some of the required variables are empty see log"
      exit 1
    fi

    if ! [ -f "${test_file}" ]; then
      log 4 "test file ${test_file} does not exist. skipping"
      continue
    fi
    # shellcheck disable=SC1090
    if ! . "${test_file}"; then
      log 3 "test ${test_name} failed"
      docker rm -f "$test_container_name" >/dev/null 2>&1
      exit 1
    else
      echo "[success] - ${test_name} ran successfully"
      docker rm -f "$test_container_name" >/dev/null 2>&1
    fi
  done

}

test() {
  if [ "${local_pipeline_canceled:-}" = "1" ]; then
    log 5 "local pipeline canceled"
    return
  fi
  if [ "${test_stage:?}" != "1" ]; then
    log 5 "test stage skipped"
    return
  fi
  hook 'pre_test'
  log 6 "stage: test"
  test_image
  hook 'post_test'
}
