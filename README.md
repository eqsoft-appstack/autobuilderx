# Autobuilderx

A workflow cli tool for syncing or building, testing and publishing images with gitlab and buildx. For specific use-cases it is also possible for building images on a local linux host with docker installed, see [INSTALLATION](./doc/INSTALLATION.md).

## Documentation

- [CONFIGURATION](./doc/CONFIGURATION.md)
- [CHECKS](./doc/CHECKS.md)
- [HOOKS](./doc/HOOKS.md)
- [TESTS](./doc/TEST.md)
- [INSTALLATION](./doc/INSTALLATION.md)
- [ARCHITECTURE](./doc/ARCHITECTURE.md)

## Quickstart

### Create your own repo by forking one of the predefined template repos

- [debian-build](https://gitlab.com/eqsoft-appstack/templates/alpine-build)
- [debian-synch](https://gitlab.com/eqsoft-appstack/templates/debian-synch)
- [alpine-build](https://gitlab.com/eqsoft-appstack/templates/alpine-build)
- [alpine-synch](https://gitlab.com/eqsoft-appstack/templates/alpine-synch)

every repo provides two branches which are populated with predefined checks and autobuilderx.cfg:

- main branch: for building or synching a container image
- upgrade branch: for os specific package upgrades 

For more details see explanations below.

For a group of repos it is recommanded to create the new repo into a subgroup for reusing CI-Variables like TARGET_REGISTRY_USER and TARGET_REGISTRY_PASSWORD. 

#### main branch in *-build
- use-case: building a custom image with Containerfile
- dependant on build scenario triggered by schedule, commit or merge_request

#### main branch in *-synch
- use-case: synching an existing image from remote to target registry without any Containerfile for building
- usually triggered by a schedule pipeline
- on private target registries like harbor there are following advantages over proxy-cache and replications:
    - checks, tests and custom hooks can be used for automated pipelines
    - os specific upgrade tag-streams with package upgrades can be splitted from target tag 

#### upgrade (upgrade branch in *-build and *-synch)
- Containerfile with os specific update and upgrade instructions
- usually triggered by a schedule pipeline

### Edit autobuilderx.cfg and .gitlab-ci.yaml

After selecting and forking a suitable template repo, customize the config params in autobuilderx.cfg and .gitlab-ci.yaml to your needs.

For details see: [CONFIGURATION](./doc/CONFIGURATION.md)

Or take a look into our generic service repos [generic service repos](https://gitlab.com/eqsoft-appstack/services/generic)

### Adding required CI Variables

If the registry of the base image is public accessible like docker.io or quay.io only two CI Variables for pushing into the target registry are required:

- TARGET_REGISTRY_USER 
- TARGET_REGISTRY_PASSWORD (masked variable)

otherwise also credential for the base registry are required:

- BASE_REGISTRY_USER 
- BASE_REGISTRY_PASSWORD (masked variable)

Checks, hooks or tests sometimes need to trigger the cancelation of the whole build pipeline for example if the scheduled base_image check does not find any changes in the base image, so nothing is to do. Therefore an additional CI Variable with a valid  personal access tokens with `api, read_api, read_repository, write_repository` scopes is required:

- GITLAB_PRIVATE_TOKEN (masked variable)

### Configuration of Pipeline Triggers

There are two basic trigger scenarios:

- scheduled trigger
- commit trigger

In special cases a merge_request is also a suitable trigger for a build pipeline:

- manual merge_request for example from a feature branch in a development workflow

- external tools are used like [renovatebot](https://github.com/renovatebot/renovate) or [dependabot](https://github.com/dependabot) for dependancy checks, which may in turn be triggered by their own scheduler

### Extended Pipeline Notifications

It is useful for scheduled pipelines to retrieve notifications not only on failed but also on successful build pipelines.

Example: a new custom image was built because a newer image behind the base tag (latest or any other major version) is available.

In gitlab activate `REPO/Settings/Integrations/Pipeline status emails` and deactivate `Notify only broken pipelines`. Fill in email addresses for `Recipients`.